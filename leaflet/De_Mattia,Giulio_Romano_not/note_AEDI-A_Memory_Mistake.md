Il brano è il primo passo di un ciclo di riflessioni sullo binomio Spazio-Timbro.

Viene posto il focus sul rapporto tra informazione e rumore. Rumore inteso come continuità in un sistema.

Informazione come suo opposto: quanti discretizzati connessi da relazioni logiche.
Lo spettro di _infiniti possibili_ tra queste due condizioni antitetiche svela la forma a spirale del discorso musicale in cui la pesatura degli estremi e delle loro declinazioni dipende dalla sensibilità, dalla larghezza e dal tempo di osservazione del sistema.