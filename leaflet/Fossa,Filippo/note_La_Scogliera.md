La Scogliera, brano acusmatico, prende voce nelle sembianze di un esame di primo anno, sul crocevia di bisogni d’espressione personali e incombenze accademiche. 
Traccia movimenti e riconfigurazioni del pensiero intorno a un oggetto apparentemente incomprensibile, in un luogo approssimabile allo stesso in cui le idee prendono forma.
