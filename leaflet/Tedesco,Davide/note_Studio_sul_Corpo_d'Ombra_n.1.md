Nel corso della nostra esistenza, ci sforziamo di sviluppare le nostre anime e le nostre menti, ma quale di esse prevarrà alla fine?
Qual è lo scopo finale dei nostri corpi senza le nostre menti ed anime, e viceversa?
Il nostro corpo è soltanto un involucro di carne per le nostre anime, ne è l'ombra o nulla di tutto ciò?
Nel tentativo di comprendere il legame che vi è tra essi è giunta sotto la mia attenzione la poesia ``Voyelles'' di Arthur Rimbaud, scintilla del brano ``Golfi d'Ombra'' del compositore Fausto Romitelli. Tale testo poetico, vanta una rilevanza musicale intrinseca che al contempo svela numerose metafore in ogni sua riga accompagnate da idee fisiche, concrete e tangibili nel solo mondo reale.
A seguito dell'individuazione di tali versi è stato realizzato un testo poetico da cui è generato il primo degli studi su tale soggetto per Flauto Basso e Live Electronics proiettato su S.T.One (altoparlante tetraedrico realizzato da Giuseppe Silvi).

Davide Tedesco
