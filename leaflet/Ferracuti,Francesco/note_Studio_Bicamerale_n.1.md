Studio Bicamerale n.1 nasce con l’intuizione di costruire uno strumento aumentato, al fine di ribaltare la catena elettroacustica, unendo il suono acustico prodotto dallo flauto e il suono elaborato elettronicamente diffuso dagli altoparlanti.

Durante il processo di creazione, sono state prese in esame alcune tecniche riprodotte direttamente all'interno dello strumento garantendo un'unicità sonora propria dell'aumentazione dello strumento
impossibili da ottenere in altro modo.
L'ascolto e ltra gli interpreti è paragonabile alla struttura bicamerale della mente umana.
