Giovanni Michelangelo D'Urso [1994] si è diplomato in Musica Elettronica al Conservatorio "Santa Cecilia" di Roma, studiando composizione con i maestri Michelangelo Lupone e Nicola Bernardini. 
Si perfeziona musicalmente frequentando l'"Accademia Musicale Chigiana" a Siena, sotto la guida dei Maestri Alvise Vidolin e Nicola Bernardini.
Ha inoltre preso parte ad innumerevoli masterclass organizzate in contesti prestigiosi e in aggiunta fa parte della comunità SEAM - Sustained ElectroAcoustic Music. 
Frequenta gli Studi Umanistici dell'Università La Sapienza.
