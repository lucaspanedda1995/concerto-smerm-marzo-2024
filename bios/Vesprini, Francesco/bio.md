Francesco Vesprini [1998] è attualmente immerso nel suo terzo anno di studi in Musica Elettronica presso il Conservatorio di Santa Cecilia di Roma. 
La sua formazione è segnata dalla guida dei Maestri Nicola Bernardini e Giampiero Gemini, con i quali approfondisce la Composizione Elettroacustica, cuore delle sue attuali sperimentazioni e creazioni artistiche.
Cerca le sue radici nello studio della sintesi musicale analogica e del pianoforte classico.
La sua musica si ispira alle arti visive, la letteratura, il teatro e alla filosofia orientale.
La sua propensione per la composizione audiovisiva integrata si sublima in un dialogo continuo tra suono e immagine che invita all'immersione sensoriale e riflessiva.